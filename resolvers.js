// import {AuthenticationError} from 'apollo-server';
import { ObjectId } from 'promised-mongo';

const { AuthenticationError, ForbiddenError } = require('apollo-server');

const { query } = require('nact');

const resource = 'resource';

module.exports = {

  Mutation: {
    changeResource: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      if (args._id) {
        return await query(collectionItemActor, { type: 'resource', search: { _id: args._id }, input: args.input }, global.actor_timeout);
      }
      return await query(collectionItemActor, { type: 'resource', input: args.input }, global.actor_timeout);
    },
    changeResourceTransaction: async (obj, args, ctx, info) => {
      //                        resource_id: new ObjectId(args.resource_id),
      //                         from: args.from,
      //                         to: args.to,
      //                         when: args.when,
      //                         changes: args.changes

      const collectionItemActor = ctx.children.get('item');
      args.resource_id = new ObjectId(args.resource_id);

      if (args._id) {
        return await query(collectionItemActor, { type: 'resource_transaction', search: { _id: args._id }, input: args.input }, global.actor_timeout);
      }
      return await query(collectionItemActor, { type: 'resource_transaction', input: args.input }, global.actor_timeout);
    },
    deleteResource: async (obj, args, ctx, info) => await ctx.db.resource.remove({ _id: new ObjectId(args.id) }),
  },

  Query: {

    getResource: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      return await query(collectionItemActor, { type: 'resource', search: { _id: args._id } }, global.actor_timeout);
    },

    getResources: async (obj, args, ctx, info) => {
      const collectionActor = ctx.children.get('collection');

      return await query(collectionActor, { type: 'resource' }, global.actor_timeout);
    },
    getResourceTransaction: async (obj, args, ctx, info) => {
      let resources = await ctx.db.resource_transaction.aggregate(
        [
          { $match: { _id: new ObjectId(args.id) } },
          {
            $lookup: {
              from: 'resource',
              localField: 'resource_id',
              foreignField: '_id',
              as: 'resource',
            },
          },
          { $limit: 1 },
          { $unwind: '$resource' },
        ],
      );

      let resource_result = resources[0];
      return resource_result || {};
    },

    getResourceTransactions: async (obj, args, ctx, info) => await ctx.db.resource_transaction.aggregate(
      [
        { $sort: { when: -1 } },
        {
          $lookup: {
            from: 'resource',
            localField: 'resource_id',
            foreignField: '_id',
            as: 'resource',
          },
        },
        { $unwind: '$resource' },
      ],
    ),
  },

};
